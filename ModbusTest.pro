TARGET = ModbusTest

QT += core gui widgets
QT += network

TEMPLATE = app

DEFINES += QT_NODLL

CONFIG += silent

CONFIG(debug, debug|release) {
	LIBS += -lmodb_lib_d
	LIBS += -L$$PWD/../modb_lib/lib
} else {
	LIBS += -lmodb_lib
	LIBS += -L$$PWD/../modb_lib/lib
}

CONFIG(debug, debug|release) {
		 DESTDIR = $$PWD/debug
		 DEFINES += DEBUG
} else {
		 DESTDIR = $$PWD/release
		 DEFINES += RELEASE
}

SOURCES += main.cpp \
    maindialog.cpp \
    PortSetting.cpp \
		qmyvalidator.cpp \
    livnyparser.cpp

HEADERS += maindialog.h \
    PortSetting.h \
		qmyvalidator.h \
    livnyparser.h

FORMS += maindialog.ui \
    PortSetting.ui

INCLUDEPATH += ../modb_lib/include
INCLUDEPATH += ../modb_lib/src
INCLUDEPATH += ../modb_lib/src/func

DEPENDPATH *= INCLUDEPATH

TRANSLATIONS  += modbustest.ts
CODEC = UTF8


win32 {
  RC_FILE = modbustest.rc
}

