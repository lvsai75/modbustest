#include "PortSetting.h"
#include "ui_PortSetting.h"
#include "../modb_lib/include/mb_global.h"

QPortSetting::QPortSetting(const QPortSet& from) :
	m_ui(new Ui::PortSetting)
{
	m_ui->setupUi(this);

	QPortSet::operator =(from);

	//m_uiBaudRate = BaudRate9600;
	//m_uiParity = ParityEven;
	m_ui->comboBoud->addItem(QString("2400"));
	m_ui->comboBoud->setItemData(0, QVariant(BaudRate2400));

	m_ui->comboBoud->addItem(QString("4800"));
	m_ui->comboBoud->setItemData(1, QVariant(BaudRate4800));

	m_ui->comboBoud->addItem(QString("9600"));
	m_ui->comboBoud->setItemData(2, QVariant(BaudRate9600));

	m_ui->comboBoud->addItem(QString("19200"));
	m_ui->comboBoud->setItemData(3, QVariant(BaudRate19200));

	m_ui->comboBoud->addItem(QString("38400"));
	m_ui->comboBoud->setItemData(4, QVariant(BaudRate38400));

	m_ui->comboBoud->addItem(QString("57600"));
	m_ui->comboBoud->setItemData(5, QVariant(BaudRate57600));

	m_ui->comboBoud->addItem(QString("115200"));
	m_ui->comboBoud->setItemData(6, QVariant(BaudRate115200));

	m_ui->comboBoud->addItem(QString("128000"));
	m_ui->comboBoud->setItemData(7, QVariant(BaudRate128000));

	m_ui->comboBoud->addItem(QString("230400"));
	m_ui->comboBoud->setItemData(8, QVariant(BaudRate230400));

	m_ui->comboBoud->addItem(QString("460800"));
	m_ui->comboBoud->setItemData(9, QVariant(BaudRate460800));
	m_ui->comboBoud->setCurrentIndex(m_ui->comboBoud->findData(QVariant(m_uiBaudRate)));

	m_ui->comboParity->addItem(QString("Even"));
	m_ui->comboParity->setItemData(0, QVariant(ParityEven));
	m_ui->comboParity->addItem(QString("None"));
	m_ui->comboParity->setItemData(1, QVariant(ParityNone));
	m_ui->comboParity->addItem(QString("Odd"));
	m_ui->comboParity->setItemData(2, QVariant(ParityOdd));
	m_ui->comboParity->addItem(QString("Mark"));
	m_ui->comboParity->setItemData(3, QVariant(ParityMark));
	m_ui->comboParity->addItem(QString("Space"));
	m_ui->comboParity->setItemData(4, QVariant(ParitySpace));

}

int QPortSetting::exec()
{
	m_ui->comboBoud->setCurrentIndex(m_ui->comboBoud->findData(m_uiBaudRate));
	m_ui->comboParity->setCurrentIndex(m_ui->comboParity->findData(m_uiParity));
	m_ui->cbSharing->setChecked(m_uiSharing);
	m_ui->ParityError->setChecked(m_uiParityErr);
	m_ui->edtSharingTCPPort->setValue(m_uiSharingTCPPort);
	return QDialog::exec();
}

QPortSetting::~QPortSetting()
{
	delete m_ui;
}

void QPortSetting::changeEvent(QEvent* e)
{
	QDialog::changeEvent(e);

	switch(e->type())
	{
	case QEvent::LanguageChange:
		m_ui->retranslateUi(this);
		break;

	default:
		break;
	}
}


void QPortSetting::on_buttonBox_clicked(QAbstractButton* button)
{
	if(m_ui->buttonBox->standardButton(button) == QDialogButtonBox::Ok)
	{
		QVariant var = m_ui->comboBoud->itemData(m_ui->comboBoud->currentIndex());
		m_uiBaudRate =  var.toInt();
		var = m_ui->comboParity->itemData(m_ui->comboParity->currentIndex());
		m_uiParity = var.toInt();
		m_uiSharing = m_ui->cbSharing->isChecked();
		m_uiParityErr = m_ui->ParityError->isChecked();
		m_uiSharingTCPPort = m_ui->edtSharingTCPPort->value();
	}
}
