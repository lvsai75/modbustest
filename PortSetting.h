#ifndef PORTSETTING_H
#define PORTSETTING_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui
{
class PortSetting;
}

class QPortSet
{
public:
	QPortSet():
		m_uiBaudRate(9600),
		m_uiParity(0),
		m_uiSharing(true),
		m_uiParityErr(true),
		m_uiSharingTCPPort(50000)
	{}
	QPortSet(const QPortSet& from)
	{
		*this = from;
	}

	QPortSet& operator =(const QPortSet& from)
	{
		m_uiBaudRate = from.m_uiBaudRate;
		m_uiParity = from.m_uiParity;
		m_uiSharing = from.m_uiSharing;
		m_uiParityErr = from.m_uiParityErr;
		m_uiSharingTCPPort = from.m_uiSharingTCPPort;
		return *this;
	}
	uint m_uiBaudRate;
	uint m_uiParity;
	bool m_uiSharing;
	bool m_uiParityErr;
	int m_uiSharingTCPPort;
};


class QPortSetting : public QDialog, public QPortSet
{
    Q_OBJECT
public:
		QPortSetting(const QPortSet& from);
    ~QPortSetting();

protected:
    void changeEvent(QEvent* e);

private:
    Ui::PortSetting* m_ui;

public slots:
    int exec();

private slots:
    void on_buttonBox_clicked(QAbstractButton* button);

};

#endif // PORTSETTING_H
