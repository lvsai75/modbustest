#include "livnyparser.h"

LivnyParser::LivnyParser() : QObject(0),
	m_state(stFindStart)
{

}

void LivnyParser::parse(QByteArray& buf)
{
	for(int i = 0; i < buf.size(); i++)
	{
		uchar b = buf[i];
		parseByte(b);
	}
}


void LivnyParser::parseByte(uchar b)
{	
	switch(m_state)
	{
	case stFindStart:
		if(b == 0x48)
		{
			m_bufPacket.append(b);
			m_state = stFindCRCStart;
		}
		break;
	case stFindCRCStart:
		if(b == 0x45)
		{
			// Совпадение контрольной суммы с 0х48, ложное начало пакета
			m_bufPacket.clear();
			m_state = stFindStart;
		}else
		{
			// Записываем байт в буфер, ищем конец пакета
			m_bufPacket.append(b);
			m_state = stFindEnd;
		}
		break;

	case stFindEnd:
		m_bufPacket.append(b);
		if(b == 0x45)
		{
			m_state = stFindCRCEnd;
		}
		break;

	case stFindCRCEnd:

		if(b == 0x45)
		{
			// Совпадение контрольной суммы с 0х45, был ложный конец пакета
			m_bufPacket.append(b);
			parsePacket();
			break;
		}else
		{
			if(b == 0x42 || b == 0x43 || b == 0x41 || b == 0x44)
			{
				// Разбираем предыдущий пакет
				parsePacket();

				m_bufPacket.append(b);
				if(b != 0x41)
				{
					// Вывод пакета с ошибкой
					parsePacket();
				}
				m_state = stFindStart;

			}else if(b == 0x48)
			{
				// Разбираем предыдущий пакет
				parsePacket();
				m_bufPacket.append(b);
				m_state = stFindCRCStart;
			}
		}
		break;
	}
}



void LivnyParser::parsePacket()
{
	QString str;

	// Анализируем пакет
	int size = m_bufPacket.count();
	if(size == 1)
	{
		if(m_bufPacket[0] == (char)0x42)
			str = "Ошибка контрольной суммы";
		else if(m_bufPacket[0] == (char)0x43)
			str = "Недопустимая функция";
		else if(m_bufPacket[0] == (char)0x44)
			str = "Неактивный пост";
	}else
	{
		// Проверка контрольной суммы
		if(!testCRC())
			str = "Ошибка контрольной суммы";
		else if(size >= 5)
		{
			if(m_bufPacket[0] == (char)0x41)
			{
				// Пакет ответа
				str = cmdToString(m_bufPacket[3]);
			}else
			{
				// Пакет запроса
				str = cmdToString(m_bufPacket[2]);
			}

		}
	}
	emit Logger(QString(m_bufPacket.toHex().toUpper()) + "   " + str, true);
	m_bufPacket.clear();
}

QString LivnyParser::cmdToString(char cmd)
{
	QString str;
	switch(cmd)
	{
	case 0x43:str = "Запрос цены";break;
	case 0x44:str = "Запрос отпущенных литров";break;
	case 0x51:str = "Заданная доза в литрах";break;
	case 0x4B:str = "Отпущенные килограммы";break;
	case 0x53:str = "Сумматор литров";break;
	case 0x54:str = "Сумматор килограммов";break;
	case 0x52:str = "Сумматор рублей";break;
	case 0x55:str = "Стоимость заданной дозы";break;
	case 0x58:str = "Чтение байта по адресу";break;
	case 0x59:str = "Чтение байта ПЗУ";break;

	case 0x63:str = "Установка цены";break;
	case 0x64:str = "Установка дозы в литрах";break;
	case 0x6B:str = "Установка дозы в килограммах";break;
	case 0x65:str = "Установка стоимости";break;
	case 0x73:str = "Команда Стоп";break;
	case 0x75:str = "Команда Продолжить";break;
	case 0x78:str = "Установка байта по адресу";break;
	}
	return str;
}

bool LivnyParser::testCRC()
{
	QByteArray crcArr = m_bufPacket;
	if(crcArr[0] == (char)0x41)
		crcArr.remove(0,1);
	uchar uchSummaResp=0;
	uchar uchSumma=0;
	int lenght = crcArr.count();
	if(lenght < 5)
		return false;
	//Считываем значение КС
	uchSummaResp = crcArr[lenght-2];

	//Обнуляем в массиве КС
	crcArr[lenght-2] = 0;

	for(uint i=0;i<lenght;i++)
		uchSumma += crcArr[i];
	//Сравниваем подсчитанный результат
	if(uchSummaResp==uchSumma)
		return true;
	return false;
}
