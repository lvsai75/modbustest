#ifndef LIVNYPARSER_H
#define LIVNYPARSER_H

#include <QObject>

class LivnyParser : public QObject
{
	Q_OBJECT
	enum packetType
	{
		ptUnknown,
		ptEmpty,
		ptWaitMoreBytes,

		ptOK,
		ptBadCRC,
		ptBadFunk
	};


	enum stateType
	{
		stFindStart,	// Ищем начало пакета
		stFindCRCStart,	// Проверка совпадения начала пакета и контрольной суммы
		stFindEnd,		// Ищем конец пакета
		stFindCRCEnd	// Проверка совпадения конца пакета и контрольной суммы
	};

public:
	LivnyParser();
	void parse(QByteArray& buf);

protected:
	void parseByte(uchar b);
	void parsePacket();
	QString cmdToString(char cmd);
	bool testCRC();

signals:
	void Logger(const QString& str, bool incTimestamp);

protected:
	int m_state;
	// Буфер формирования пакета для вывода в лог
	QByteArray m_bufPacket;	
};



#endif // LIVNYPARSER_H
