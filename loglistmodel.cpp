#include "loglistmodel.h"

LogListModel::LogListModel(QObject* parent) :
	QAbstractListModel(parent), m_maxSize(1000),m_removeRow(0),
	m_addRow(0)
{
	connect(&m_timerUpdate, SIGNAL(timeout()), SLOT(updateModel()));
	m_timerUpdate.setSingleShot(true);
	m_timerUpdate.setInterval(500);
}

void LogListModel::addString(QString str)
{
	int row = m_lst.count();
	int delta = row - m_maxSize;
	if(delta > 0)
	{
		for(int i = 0;i < delta;i++)
			m_lst.removeAt(i);
	}
	m_lst.append(str);
	if(!m_timerUpdate.isActive())
		m_timerUpdate.start();
}

void LogListModel::setMaxSize(int max)
{
	m_maxSize = max;
}

void LogListModel::updateModel()
{
	beginResetModel();
	endResetModel();
}


int LogListModel::rowCount(const QModelIndex& parent) const
{
	if (parent.isValid())
		return 0;
	return m_lst.count();
}

QVariant LogListModel::data(const QModelIndex& index, int role) const
{
	if (index.row() < 0 || index.row() >= m_lst.size())
		return QVariant();

	if (role == Qt::DisplayRole || role == Qt::EditRole)
		return m_lst.at(index.row());

	return QVariant();
}
