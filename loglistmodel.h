#ifndef LOGLISTMODEL_H
#define LOGLISTMODEL_H

#include <QAbstractListModel>
#include <QTimer>

class LogListModel : public QAbstractListModel
{
	Q_OBJECT
public:
	LogListModel(QObject *parent = 0);

	void addString(QString str);
	void clear(){m_lst.clear();}
	void setMaxSize(int max);


public slots:
	void updateModel();

public:
	QStringList m_lst;
	int m_maxSize;
	int m_removeRow;
	int m_addRow;

	QTimer m_timerUpdate;

	// QAbstractItemModel interface
public:
	virtual int rowCount(const QModelIndex& parent) const;
	virtual QVariant data(const QModelIndex& index, int role) const;


};

#endif // LOGLISTMODEL_H
