#include <QApplication>
#include "maindialog.h"
#include <QTextCodec>
#include <QStyle>
#include <QStyleFactory>
#include <QTranslator>

int main(int argc, char* argv[])
{
	QApplication a(argc, argv);
	QCoreApplication::setOrganizationName("Prompribor");
	QCoreApplication::setApplicationName("ModbusTest");
	QTextCodec* codec = QTextCodec::codecForName("UTF-8");
	QTextCodec::setCodecForLocale(codec);

	{
		QTranslator* translator = new QTranslator(&a);
		if(translator->load("modbustest.qm"))
			a.installTranslator(translator);
	}

	{
		QTranslator* translator = new QTranslator(&a);
		if(translator->load("modb_lib.qm"))
			a.installTranslator(translator);
	}
	Dialog w;
	w.setWindowFlags(Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint);
	w.setWindowTitle(QString("ModbusTest [%1]").arg(QObject::trUtf8("сборка от %1 в %2").arg(__DATE__, __TIME__)));
	w.show();
	return a.exec();
}
