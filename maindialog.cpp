#include "maindialog.h"
#include "ui_maindialog.h"
#include "fancylineedit.h"
#include "readfunc.h"
#include "writefunc.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QHostAddress>


//=============================================================================

QString itox(const QString& str)
{
	return QString().sprintf("%X", str.toInt());
}

QString xtoi(const QString& str)
{
	int x;
	sscanf(str.toLatin1(), "%X", &x);
	return QString::number(x);
}

bool scanHEX(const QString& str)
{
	for(int i = 0; i < str.length(); i++)
		if(!isxdigit(str[i].toLatin1())) { return false; }

	return true;
}

bool scanDEC(const QString& str)
{
	for(int i = 0; i < str.length(); i++)
		if(!isdigit(str[i].toLatin1())) { return false; }

	return true;
}

QString WordToStr(const QByteArr& arr)
{
	QString str, buff;

	for(int i = 0; i < arr.size(); i++)
	{
		buff.sprintf("%.02X", arr.at(i));
		str.append(buff);
	}

	str.resize(arr.size() * 2);
	return str;
}

QString WordToStr(QWordArray& arr)
{
	QString str, buff;

	for(int i = 0; i < arr.size(); i++)
	{
		buff.sprintf("%.04X", arr.at(i));
		str.append(buff);
	}

	str.resize(arr.size() * 4);
	return str;
}

int StrToArr(QString& str, QByteArr& arr)
{
	int iSize = str.size();

	if(iSize % 4)
	{
		return 1;
	}

	arr.resize(iSize / 2);

	for(int i = 0; i < iSize / 2; i++)
	{
		bool ok;
		ulong hex = str.mid(i * 2, 2).toULong(&ok, 16);

		if(!ok)
		{
			return 1;
		}

		arr[i] = (quint16)hex;
	}

	return 0;
}

Dialog::Dialog(QWidget* parent)
	: QDialog(parent), ui(new Ui::Dialog), m_logFile(0)
{
	ui->setupUi(this);
	m_Modbus = 0;
	setLayout(ui->verticalLayout);

	QStringList ports;
	EnumerateSerialPorts(ports);
	int iSize = ports.size();

	for(int k = 0; k < iSize; k++)
	{
		ui->comboPort->addItem(ports[k]);
	}

	ui->comboPort->installEventFilter(this);

	QVariant mode;
	ui->comboBox_8->addItem(tr("Эхо"));
	mode = 0;
	ui->comboBox_8->setItemData(0, mode);
	ui->comboBox_8->addItem(tr("Сброс счетчиков"));
	mode = 0x000A0000;
	ui->comboBox_8->setItemData(1, mode);
	ui->comboBox_8->addItem(tr("Количество пакетов"));
	mode = 0x000B0000;
	ui->comboBox_8->setItemData(2, mode);
	ui->comboBox_8->addItem(tr("Ошибки CRC"));
	mode = 0x000C0000;
	ui->comboBox_8->setItemData(3, mode);
	ui->comboBox_8->addItem(tr("Ошибки шины"));
	mode = 0x000D0000;
	ui->comboBox_8->setItemData(4, mode);
	ui->comboBox_8->addItem(tr("Slave message count"));
	mode = 0x000E0000;
	ui->comboBox_8->setItemData(5, mode);
	ui->comboBox_8->addItem(tr("Slave No response count"));
	mode = 0x000F0000;
	ui->comboBox_8->setItemData(6, mode);
	ui->comboBox_8->addItem(tr("Slave NAK count"));
	mode = 0x00100000;
	ui->comboBox_8->setItemData(7, mode);
	//Скрываем контрол счетчиков статистики
	ui->lineEdit_Count->setVisible(false);
	ui->label_8->setVisible(false);
	QSettings settings;
	QVariant com = settings.value("port/num");
	ui->comboPort->setCurrentIndex(ui->comboPort->findText(com.toString()));
	bool rez = true;
	com = settings.value("port/address", QVariant(1));
	ui->Address->setText(com.toString());
	com = settings.value("port/number", QVariant(1));
	ui->lineAddrReg->setText(com.toString());
	ui->lineColReg->setText(settings.value("port/count", QVariant(1)).toString());
	ui->lineAddrReg_2->setText(settings.value("port/number_2", QVariant(1)).toString());
	ui->lineColReg_2->setText(settings.value("port/count_2", QVariant(1)).toString());
	ui->edtRouterAddress->setText(settings.value("port/routerAddress", QVariant(1)).toString());
	m_portSet.m_uiBaudRate = settings.value("port/baudrate", QVariant(BaudRate9600)).toInt(&rez);
	m_portSet.m_uiParity = settings.value("port/parity", QVariant(ParityEven)).toInt(&rez);

	m_portSet.m_uiSharing = settings.value("port/sharing", true).toBool();
	m_portSet.m_uiSharingTCPPort = settings.value("port/tcpPort", 50000).toInt(&rez);

	com = settings.value("port/funcEdit", QVariant("03"));
	ui->funcEdit->setText(com.toString());
	com = settings.value("port/dataEdit", QVariant(""));
	ui->dataEdit->setText(com.toString());

	QVariant parser = settings.value("parser");
	ui->comboBox_Parser->setCurrentIndex(parser.toInt());

	ui->tabWidget->setCurrentIndex(settings.value("lastTab", 0).toInt());


	m_Scrool = Qt::Checked;
	m_OneReg = Qt::Unchecked;
	ui->checkScrList->setCheckState(m_Scrool);
	on_cboxRouteType_activated(0);
	RefreshLblPort();
	//Устанавливаем валидаторы
	pHexValidate = new QHexValidator(this);
	pDecValidate = new QDecValidator(this);
	ui->Address->setValidator(pDecValidate);
	ui->lineAddrReg->setValidator(pDecValidate);
	ui->lineColReg->setValidator(pDecValidate);
	ui->lineAddrReg_2->setValidator(pDecValidate);
	ui->lineColReg_2->setValidator(pDecValidate);
	ui->lineValue_2->setValidator(pHexValidate);
	ui->funcEdit->setValidator(pHexValidate);
	ui->dataEdit->setValidator(pHexValidate);
	ui->lineEditMaxLog->setValidator(pDecValidate);
	ui->edtLogFileName->setText(settings.value("logFileName").toString());
	ui->checkBoxLogFile->setChecked(settings.value("logFileUse").toBool());
	loadHistory();
	ui->checkHex->setChecked(true);
	on_checkHex_clicked();
	ui->checkHex_2->setChecked(true);
	on_checkHex_2_clicked();
	m_prevPacketType = ptUnknown;
	m_prevSize = 0;
	logFileOpen();
	//FancyLineEdit* pEdit = new FancyLineEdit(this);
	//ui->MyLayout->addWidget(pEdit);

	connect(&m_livParser, SIGNAL(Logger(QString,bool)), SLOT(Logger(QString,bool)));
	ui->listView->setModel(&m_logModel);
	m_logModel.setMaxSize(ui->lineEditMaxLog->text().toInt());
}


Dialog::~Dialog()
{
	QSettings settings;
	QVariant com =  ui->comboPort->itemText(ui->comboPort->currentIndex());
	settings.setValue("port/num", com);

	if(!ui->checkHex_3->isChecked())
	{
		com = ui->Address->text();
	}
	else
	{
		com = xtoi(ui->Address->text());
	}

	settings.setValue("port/address", com);

	if(!ui->checkHex->isChecked())
	{
		com = ui->lineAddrReg->text();
	}
	else
	{
		com = xtoi(ui->lineAddrReg->text());
	}

	settings.setValue("port/number", com);
	com =  ui->lineColReg->text();
	settings.setValue("port/count", com);

	if(!ui->checkHex_2->isChecked())
	{
		com = ui->lineAddrReg_2->text();
	}
	else
	{
		com = xtoi(ui->lineAddrReg_2->text());
	}

	settings.setValue("port/number_2", com);
	settings.setValue("port/count_2", ui->lineColReg_2->text());
	settings.setValue("port/routerAddress", ui->edtRouterAddress->text());
	settings.setValue("port/baudrate", m_portSet.m_uiBaudRate);
	settings.setValue("port/parity", m_portSet.m_uiParity);
	settings.setValue("port/sharing", m_portSet.m_uiSharing);
	settings.setValue("port/tcpPort", m_portSet.m_uiSharingTCPPort);
	settings.setValue("port/funcEdit", ui->funcEdit->text());
	settings.setValue("port/dataEdit", ui->dataEdit->text());
	settings.setValue("logFileName", ui->edtLogFileName->text());
	settings.setValue("logFileUse", ui->checkBoxLogFile->isChecked());	

	QVariant parser = ui->comboBox_Parser->currentIndex();
	settings.setValue("parser", parser);

	//Сохраняем активную закладку
	settings.setValue("lastTab", ui->tabWidget->currentIndex());


	saveHistory();
	delete ui;
}

bool Dialog::eventFilter(QObject* obj, QEvent* event)
{

	if(obj == ui->comboPort)
	{
		if(event->type() == QEvent::MouseButtonPress)
		{
			ui->comboPort->clear();

			QStringList ports;
			EnumerateSerialPorts(ports);
			int iSize = ports.size();

			ports.sort();

			for(int k = 0; k < iSize; k++)
			{
				ui->comboPort->addItem(ports[k]);
			}
		}
	}

	return QDialog::eventFilter(obj, event);
}

void Dialog::addToHistory(const QString& str)
{
	//qDebug(str.toAscii());
	QVectorIterator<QString> iter(m_History);
	QString tmp, cmt;
	bool found = false;

	while(iter.hasNext())
	{
		tmp = iter.next();
		cmt = tmp.section('\t', 1, 1);
		tmp = tmp.section('\t', 0, 0);

		if(tmp == str) { found = true; }
	}

	if(!found)
	{
		m_History.push_back(str);
		ui->listHistory->addItem(str);
	}
}

void Dialog::saveHistory()
{
	QFile f_hist("History.txt");
	f_hist.open(QIODevice::WriteOnly);
	QVectorIterator<QString> iter(m_History);
	QString tmp, cmt;

	while(iter.hasNext())
	{
		tmp = iter.next();
		cmt = tmp.section('\t', 1, 1);
		tmp = tmp.section('\t', 0, 0);
		f_hist.write(tmp.toLatin1() + "\n");
		f_hist.write(cmt.toLatin1() + "\n");
	}

	f_hist.close();
}

void Dialog::loadHistory()
{
	if(QFile::exists("History.txt"))
	{
		QFile f_hist("History.txt");
		f_hist.open(QIODevice::ReadOnly);
		QString tmp, cmt;

		while(!f_hist.atEnd())
		{
			tmp = f_hist.readLine();
			cmt = f_hist.readLine();
			tmp = tmp.left(tmp.length() - 1);
			cmt = cmt.left(cmt.length() - 1);
			tmp += "\t" + cmt;
			addToHistory(tmp);
		}
	}
}

void Dialog::Logger(const QString& str, bool incTimestamp)
{
	QString s = str;
	m_logFile->log(s);
	if(incTimestamp)
	{
		s = QString("%1 %2").arg(
					QDateTime::currentDateTime().toString("yy.MM.dd.hh.mm.ss.zzz"),
					str);
	}

	m_logModel.addString(s);

	if(ui->checkScrList->isChecked())
	{
		ui->listView->scrollToBottom();		
	}
}

void Dialog::RefreshLblPort(void)
{
	QString parity;
	switch(m_portSet.m_uiParity)
	{
	case ParityNone:	parity = "N";	break;
	case ParityOdd: parity = "O"; break;
	case ParityEven: parity = "E"; break;
	case ParityMark: parity = "M"; break;
	case ParitySpace: parity = "S"; break;
	default: parity = "N";
	}
	QString label = QString("%1-%2").arg(m_portSet.m_uiBaudRate).arg(parity);
	if(m_portSet.m_uiSharing)
		(label += "-") += QString::number(m_portSet.m_uiSharingTCPPort);
	ui->portSetting->setText(label);
}

void Dialog::on_pushOpen_clicked()
{
	closePort();
	m_sNumPort = ui->comboPort->itemText(ui->comboPort->currentIndex());
	m_Modbus = QMBSyncPort::New(this);
	connect(m_Modbus, SIGNAL(error(QString,ModbusStatus)), SLOT(onError(QString)));
	switch(ui->cboxRouteType->currentIndex())
	{
	case 0:
	case 1:
		connect(m_Modbus, SIGNAL(onRTURequest(QString)), SLOT(onRTURequest(QString)));
		connect(m_Modbus, SIGNAL(onRTUResponse(QString)), SLOT(onRTUResponse(QString)));
		m_Modbus->setTimeoutMs(ui->edtTimeout->value());
		m_Modbus->setReplaceErrChar(m_portSet.m_uiParityErr);
		m_Modbus->setSharingPort(m_portSet.m_uiSharing ? m_portSet.m_uiSharingTCPPort : -1);
		m_Modbus->Init(m_sNumPort, m_portSet.m_uiBaudRate, m_portSet.m_uiParity);
		break;
	case 2:
	{
		QTCPAddrCfg cfg;
		cfg.m_type = QTCPAddrCfg::atIP;
		cfg.m_ip_start = QHostAddress(ui->edtRouterAddressTCP->text()).toIPv4Address();
		cfg.m_port_start = 502;
		cfg.m_from = 0;
		cfg.m_to = 0;
		m_Modbus->addTcpAddrCfg(&cfg);
	}
	{
		QTCPAddrCfg cfg;
		cfg.m_type = QTCPAddrCfg::atSubnet;
		cfg.m_ip_start = QHostAddress(ui->edtRouterAddressTCP->text()).toIPv4Address();
		cfg.m_port_start = 502;
		cfg.m_from = 1;
		cfg.m_to = 247;
		m_Modbus->addTcpAddrCfg(&cfg);
	}
		connect(m_Modbus, SIGNAL(onTCPRequest(quint32,QString)), SLOT(onTCPRequest(quint32,QString)));
		connect(m_Modbus, SIGNAL(onTCPResponse(quint32,QString)), SLOT(onTCPResponse(quint32,QString)));
		m_sNumPort = "Modbus TCP";
		m_Modbus->InitTCP();
		break;
	}
	if(m_Modbus->isPortFatalError())
	{
		delete m_Modbus; m_Modbus = 0;
	}
	else
		Logger(tr("Успешно открыт порт ") + m_sNumPort);
}

void Dialog::onRTURequest(const QString req)
{
	Logger(tr("Запрос %1").arg(req));
}

void Dialog::onRTUResponse(const QString req)
{
	Logger(tr("Ответ %1").arg(req));
}

void Dialog::onTCPRequest(quint32 peerIP, const QString& recv)
{
	Logger(tr("Запрос %1 %2").arg(QHostAddress(peerIP).toString(), recv));

}

void Dialog::onTCPResponse(quint32 peerIP, const QString& resp)
{
	Logger(tr("Ответ %1 %2").arg(QHostAddress(peerIP).toString(), resp));
}

void Dialog::onError(const QString req)
{
	Logger(tr("Ошибка: %1").arg(req));
}

bool Dialog::chkPort()
{
	if(!m_Modbus)
	{
		on_pushOpen_clicked();
		if(!m_Modbus)
			return false;
	}
	switch(ui->cboxRouteType->currentIndex())
	{
	case 0:
		if(!m_Modbus->rtuMode())
			on_pushOpen_clicked();
		if(!m_Modbus->m_requestRoutePrefix.isEmpty())
		{
			m_Modbus->m_requestRoutePrefix.clear();
			m_Modbus->m_responceRoutePrefix.clear();
		}
		break;
	case 1:
	{
		if(!m_Modbus->rtuMode())
			on_pushOpen_clicked();
		int size = ui->cbRouteIncPort->isChecked() ? 3 : 2;
		if(m_Modbus->m_requestRoutePrefix.size() != size)
			m_Modbus->m_requestRoutePrefix.resize(size);
		m_Modbus->m_requestRoutePrefix[0] = ui->edtRouterAddress->text().toInt();
		m_Modbus->m_requestRoutePrefix[1] = 0x42;
		if(size >= 3) // include port number
			m_Modbus->m_requestRoutePrefix[2] = ui->edtRouterPort->text().toInt();
		m_Modbus->m_responceRoutePrefix = m_Modbus->m_requestRoutePrefix;
		break;
	}
	case 2:
	{
		if(m_Modbus->rtuMode())
			on_pushOpen_clicked();
	}
	}
	return true;
}

uint Dialog::Read(void)
{
	if(!chkPort())
		return 1;

	ushort ushAddr = 1;
	ushort ushReg = 1;
	ushort ushNumber = 1;
	bool rez = true;
	QString tmp, buf;
	QWordArray anRegValues;

	if(ui->checkHex_3->isChecked())
	{
		ushAddr = xtoi(ui->Address->text()).toUShort(&rez, 10);
	}
	else
	{
		ushAddr = ui->Address->text().toUShort(&rez, 10);
	}

	if(ui->checkHex->isChecked())
	{
		ushReg = xtoi(ui->lineAddrReg->text()).toUShort(&rez, 10);
	}
	else
	{
		ushReg = ui->lineAddrReg->text().toUShort(&rez, 10);
	}

	ushNumber = ui->lineColReg->text().toUShort(&rez, 10);
	anRegValues.resize(ushNumber);
	ui->lineValue->setText("");

	buf.sprintf("%.02X03%.04X%.04X", ushAddr, ushReg, ushNumber);
	tmp.append(buf);
	addToHistory(tmp);
	QScopedPointer<QFuncReadHoldingReg> f(QFuncReadHoldingReg::New(ushAddr, ushReg, ushNumber, m_Modbus, 0));
	if(f->execute() > mbbNotOk)
	{
		Logger(tr("Ошибка чтения - ") + m_Modbus->getLastErrorString());
		return 0;
	}
	ui->lineValue->setText(WordToStr(f->response()));
	return 0;
	//Logger(tr("Неверные данные"));
	//return 0;
}

QWordArray arrToWordArray(const QByteArr& arr)
{
	QWordArray r;
	for(int i = 0; i < arr.size() / 2; i++)
	{
		r << ((quint16)(arr[i * 2]) | ((quint16)(arr[i * 2 + 1]) << 8));
	}
	return r;
}

QByteArr wordArrayToArr(const QWordArray& arr)
{
	QByteArr r;
	for(int i = 0; i < arr.size(); i++)
	{
		r << (arr[i] & 0xFF);
		r << (arr[i] >> 8);
	}
	return r;
}

QByteArray arrToArray(const QByteArr& arr)
{
	return QByteArray((const char*)arr.constData(), arr.size());
}

QByteArr arrayToArr(const QByteArray& arr)
{
	QByteArr r;
	r.resize(arr.size());
	memcpy(r.data(), arr.constData(), arr.size());
	return r;
}

uint Dialog::Write(void)
{
	QString sError;
	if(!chkPort())
		return 1;

	ushort ushAddr = 1;
	ushort ushReg = 1;
	ushort ushNumber = 1;
	bool rez = true;
	QString tmp, buf;
	QByteArr anRegValues;
	bool isSingle = (ui->checkBox_Singl->isChecked()) && (ui->lineColReg_2->text().toInt() == 1);

	if(ui->checkHex_3->isChecked())
	{
		ushAddr = xtoi(ui->Address->text()).toUShort(&rez, 10);
	}
	else
	{
		ushAddr = ui->Address->text().toUShort(&rez, 10);
	}

	//    if(!rez)
	//        goto err;
	if(ui->checkHex_2->isChecked())
	{
		ushReg = xtoi(ui->lineAddrReg_2->text()).toUShort(&rez, 10);
	}
	else
	{
		ushReg = ui->lineAddrReg_2->text().toUShort(&rez, 10);
	}

	//    if(!rez)
	//        goto err;
	ushNumber = ui->lineColReg_2->text().toUShort(&rez, 10);
	//    if(!rez)
	//        goto err;
	sError = ui->lineValue_2->text();

	if(StrToArr(sError, anRegValues))
	{
		goto err;
	}

	if(anRegValues.size() != ushNumber * 2)
	{
		goto err;
	}

	if(isSingle)
	{
		buf.sprintf("%.02X06%.04X%s", ushAddr, ushReg, ui->lineValue_2->text().toStdString().c_str());
	}
	else
	{
		buf.sprintf("%.02X10%.04X%.04X%s", ushAddr, ushReg, ushNumber, ui->lineValue_2->text().toStdString().c_str());
	}

	tmp.append(buf);
	addToHistory(tmp);

	{
		QByteArray data = byteArrToArray(anRegValues);
		QScopedPointer<QFuncWriteHoldingReg> f(QFuncWriteHoldingReg::New(ushAddr, ushReg, data, isSingle, m_Modbus, 0));
		if(f->execute() > mbbNotOk)
			Logger(tr("Ошибка записи - ") + m_Modbus->getLastErrorString());
	}

	return 0;
err:
	Logger(tr("Неверные данные"));
	return 0;
}


void Dialog::customTest()
{
	if(!chkPort())
		return;

	QByteArray abyQuery;
	QByteArr abyReply;
	bool ok;

	if(ui->checkHex_3->isChecked())
	{
		abyQuery.push_back((ui->Address->text()).toUShort(&ok, 16));    //ADDR
	}
	else
	{
		abyQuery.push_back((ui->Address->text()).toUShort(&ok, 10));    //ADDR
	}

	abyQuery.push_back((ui->funcEdit->text()).toUShort(&ok, 16)); //Function

	for(int i = 0; i < ui->dataEdit->text().length() / 2; i++)
	{
		abyQuery.push_back(ui->dataEdit->text().mid(i * 2, 2).toUShort(&ok, 16));
	}

	if((ui->dataEdit->text().length() % 2))
	{
		abyQuery.push_back(ui->dataEdit->text().mid(ui->dataEdit->text().length() - 1, 1).toUShort(&ok, 16));
	}

	QString tmp, buf;

	for(int i = 0; i < abyQuery.count(); i++)
	{
		buf.sprintf("%.02X", (uchar)abyQuery.at(i));
		tmp.append(buf);
	}

	addToHistory(tmp);
	ModbusStatus st = m_Modbus->TxRxMessage(byteArrayToArr(abyQuery), abyReply);
	if(st > mbbNotOk)
		Logger(tr("Ошибка - %1").arg(m_Modbus->getLastErrorString()));
}

uint Dialog::LoopTest(void)
{
	if(!chkPort())
		return 1;

	bool rez = false;
	QVariant subFunk = 0;
	ushort ushAddr = 1;
	uint uiSubFunk = 0;
	quint16 data = 0;
	QByteArr query;

	if(ui->checkHex_3->isChecked())
	{
		ushAddr = xtoi(ui->Address->text()).toUShort(&rez, 10);
	}
	else
	{
		ushAddr = ui->Address->text().toUShort(&rez, 10);
	}

	if(!rez)
	{
		goto err;
	}

	subFunk = ui->comboBox_8->itemData(ui->comboBox_8->currentIndex());
	uiSubFunk = subFunk.toUInt(&rez);

	if(!rez)
	{
		goto err;
	}

	query << ushAddr;
	query << 0x08;
	query << (uiSubFunk >> 8);
	query << (uiSubFunk & 0xFF);
	query << (data >> 8);
	query << (data & 0xFF);

	{
		QByteArr resp;
		ModbusStatus error = m_Modbus->TxRxMessage(query, resp);

		if(error > mbbNotOk)
		{
			Logger(tr("Ошибка - ") + m_Modbus->getLastErrorString());
			return 0;
		}
	}

	return 0;
err:
	Logger(tr("Неверные данные"));
	return 0;
}

void Dialog::on_pushClose_clicked()
{
	ui->cbListeningOn->setChecked(false);
	ui->cbTimerCustomRequest->setChecked(false);
	ui->cbTimerLoopTest->setChecked(false);
	ui->cbTimerRead->setChecked(false);
	ui->cbTimerWrite->setChecked(false);
	closePort();
}

void Dialog::closePort()
{
	if(m_Modbus)
	{
		QString error;
		delete m_Modbus; m_Modbus = 0;
		error = tr("Закрыт порт ") + m_sNumPort;
		Logger(error);
	}
}


void Dialog::on_pushButton_pressed()
{
	Read();
}

void Dialog::on_checkScrList_stateChanged(int)
{
	m_Scrool = ui->checkScrList->checkState();
}

void Dialog::on_pushButton_2_clicked()
{
	m_logModel.clear();
}

void Dialog::on_pushButton_Setup_clicked()
{
	QPortSetting pDlg(m_portSet);

	if(pDlg.exec())
	{
		m_portSet = pDlg;
		if(m_Modbus)
			on_pushOpen_clicked();
	}

	RefreshLblPort();
}

packetType parsePacket(QByteArray& buf, packetType prevPacketType, int prevPos, int pos, int size)
{
	if(buf[pos + 1] == (char)3 && size == 8 && (prevPacketType == ptUnknown || prevPacketType == ptAnswer))
		return ptRequest;
	if(buf[pos + 1] == (char)3
		 && (prevPacketType == ptRequest)
		 && buf[prevPos + 1] == (char)3
		 && buf[prevPos] == buf[pos]
		 && size == buf[pos + 2] + (char)5)
		return ptAnswer;
	return ptUnknown;
}

packetType parseModbusRtu(QByteArray& buf, packetType prevPacketType, int prevPos, int pos, int& size)
{
	if(buf.isEmpty())
		return ptEmpty;
	size = buf.size() - pos;
	if(size < 6)
		return ptWaitMoreBytes;
	quint8* puchMsg = ((quint8*)buf.constData()) + pos;
	if(size >= 256)
		return ptUnknown;
	if(modb_lib::CalcCrcFast(puchMsg, size) == 0) // CRC ok
	{
		return parsePacket(buf, prevPacketType, prevPos, pos, size);
	}
	for(int ii = 0; ii < 50; ii++)
		for(int i = 6; (i < size - ii) && (i < 256); i++)
			if(modb_lib::CalcCrcFast(puchMsg + ii, i) == 0) // CRC ok
			{
				if(ii == 0)
				{
					size = i;
					return parsePacket(buf, prevPacketType, prevPos, pos, size);
				}
				else
				{
					size = ii;
					return ptUnknown;
				}
			}
	return ptUnknown;
}



void Dialog::portListen()
{
	if(!chkPort())
		return;

	m_Modbus->readRawData(m_listeningBuf);

	if(ui->comboBox_Parser->currentIndex() == 1)
	{
		m_livParser.parse(m_listeningBuf);
		m_listeningBuf.clear();
		return;
	}

	int size = m_prevSize;	
	forever
	{
		packetType pType = parseModbusRtu(m_listeningBuf, m_prevPacketType, 0, m_prevSize, size);
		QString templ;
		switch (pType) {
		case ptEmpty:	case ptWaitMoreBytes:	return;
		case ptUnknown:
			templ = tr("Пакет %1");
			break;
		case ptAnswer:
			templ = tr("Ответ %1");
			break;
		case ptRequest:
			templ = tr("Запрос %1");
			break;
		}
		Logger(templ.arg(QString(m_listeningBuf.mid(m_prevSize, size).toHex().toUpper())), true);
		m_listeningBuf.remove(0, m_prevSize);
		m_prevPacketType = pType;
		m_prevSize = size;
	}
}






void Dialog::on_pushButton_3_clicked()
{
	Write();
}

void Dialog::on_checkBox_Singl_stateChanged(int)
{
	m_OneReg = ui->checkBox_Singl->checkState();

	if(m_OneReg == Qt::Checked)
	{
		ui->lineColReg_2->setEnabled(false);
		ui->lineColReg_2->setText(QString("1"));
	}
	else
	{
		ui->lineColReg_2->setEnabled(true);
	}
}

void Dialog::on_pushButton_4_pressed()
{
	LoopTest();
}

void Dialog::on_comboBox_8_currentIndexChanged(int index)
{
	if(index >= 2 && index <= 7)
	{
		ui->lineEdit_Count->setVisible(true);
		ui->label_8->setVisible(true);
	}
	else
	{
		ui->lineEdit_Count->setVisible(false);
		ui->label_8->setVisible(false);
	}
}

void Dialog::on_tabWidget_currentChanged(int index)
{
	Q_UNUSED(index);
	ui->cbListeningOn->setChecked(false);
	ui->cbTimerCustomRequest->setChecked(false);
	ui->cbTimerLoopTest->setChecked(false);
	ui->cbTimerRead->setChecked(false);
	ui->cbTimerWrite->setChecked(false);
}

void Dialog::on_checkHex_clicked()
{
	QString tmp;

	if(ui->checkHex->isChecked())
	{
		ui->lineAddrReg->setValidator(pHexValidate);
		tmp = itox(ui->lineAddrReg->text());
		ui->lineAddrReg->setText(tmp);
	}
	else
	{
		tmp = xtoi(ui->lineAddrReg->text());
		ui->lineAddrReg->setText(tmp);
		ui->lineAddrReg->setValidator(pDecValidate);
	}
}

void Dialog::on_checkHex_2_clicked()
{
	QString tmp;

	if(ui->checkHex_2->isChecked())
	{
		ui->lineAddrReg_2->setValidator(pHexValidate);
		tmp = itox(ui->lineAddrReg_2->text());
		ui->lineAddrReg_2->setText(tmp);
	}
	else
	{
		tmp = xtoi(ui->lineAddrReg_2->text());
		ui->lineAddrReg_2->setText(tmp);
		ui->lineAddrReg_2->setValidator(pDecValidate);
	}
}

void Dialog::on_userFuncButton_clicked()
{
	customTest();
}

void Dialog::on_checkHex_3_clicked()
{
	QString tmp;

	if(ui->checkHex_3->isChecked())
	{
		ui->Address->setValidator(pHexValidate);
		tmp = itox(ui->Address->text());
		ui->Address->setMaxLength(2);
		ui->Address->setText(tmp);
	}
	else
	{
		tmp = xtoi(ui->Address->text());
		ui->Address->setMaxLength(3);
		ui->Address->setText(tmp);
		ui->Address->setValidator(pDecValidate);
	}
}

void Dialog::on_listHistory_itemDoubleClicked(QListWidgetItem* item)
{
	//if(item->text().midRef(2, 2).toString() == "42") { return; }

	QString hist = item->text().section('\t', 0, 0);
	QString command = hist.midRef(2, 2).toString();
	int i = 3;
	if(command == "03")
		i = 1;
	else if(command == "10" || command == "06")
		i = 2;

	if(ui->checkHex_3->isChecked())
		ui->Address->setText(hist.left(2));
	else
		ui->Address->setText(xtoi(hist.left(2)));

	ui->tabWidget->setCurrentIndex(i);
	switch(i)
	{
	// Вкладка чтения
	case 1:
		// адрес регистра
		if(ui->checkHex->isChecked())
			ui->lineAddrReg->setText(hist.midRef(4, 4).toString());
		else
			ui->lineAddrReg->setText(xtoi(hist.midRef(4, 4).toString()));

		ui->lineColReg->setText(xtoi(hist.midRef(8, 4).toString()));
		break;

		// Вкладка записи
	case 2:
		// адрес регистра
		if(ui->checkHex_2->isChecked())
			ui->lineAddrReg_2->setText(hist.midRef(4, 4).toString());
		else
			ui->lineAddrReg_2->setText(xtoi(hist.midRef(4, 4).toString()));

		// кол-во регистров
		if(command == "06")
		{
			ui->lineColReg_2->setText("1");
			ui->lineValue_2->setText(hist.midRef(8).toString());
			ui->checkBox_Singl->setChecked(true);
		}
		else if(command == "10")
		{
			ui->lineColReg_2->setText(xtoi(hist.midRef(8, 4).toString()));
			ui->lineValue_2->setText(hist.midRef(12).toString());
			ui->checkBox_Singl->setChecked(false);
		}
		break;

		// Свой ввод
	case 3:
		ui->funcEdit->setText(hist.midRef(2, 2).toString());
		ui->dataEdit->setText(hist.midRef(4).toString());
		break;

	default:
		break;
	}
}

void Dialog::on_listHistory_itemClicked(QListWidgetItem* item)
{
	ui->lineEdit_Edit->setText(item->text().section('\t', 1, 1));
}

void Dialog::on_pushButton_Edit_clicked()
{
	if(m_History.count() == 0) { return; }

	QListWidgetItem* w = ui->listHistory->currentItem();
	if(!w)
		return;
	QString tmp = w->text();
	tmp = tmp.section('\t', 0, 0);
	tmp += "\t" + ui->lineEdit_Edit->text();
	w->setText(tmp);
	m_History[ui->listHistory->currentRow()] = tmp;
}

void Dialog::on_pushButton_5_clicked()
{

	QModelIndexList indexes = ui->listView->selectionModel()->selectedIndexes();
	QString str;

	foreach (QModelIndex index, indexes)
		str += m_logModel.data(index,Qt::DisplayRole).toString() + "\n";

	QApplication::clipboard()->setText(str);
}

void Dialog::on_edtTimeout_valueChanged(int)
{
	if(m_Modbus)
		m_Modbus->setTimeoutMs(ui->edtTimeout->value());
}

void Dialog::logFileOpen()
{	
	if(m_logFile)
	{
		delete m_logFile;
		m_logFile = NULL;
	}
	QString logFName = ui->edtLogFileName->text();

	if(!logFName.isEmpty() && ui->checkBoxLogFile->isChecked())
	{
		m_logFile = new QLogFile("modbustest", logFName);
		m_logFile->setMaxFileSize(100E+6);
		m_logFile->setMaxTotalFileSize(2.1E+9);
		m_logFile->setMaxFileCnt(20);
	}
}

void Dialog::on_pushButtonLogFileSelect_clicked()
{
	QString logFName = QFileDialog::getExistingDirectory(
				this, tr("Укажите каталог хранения лог-файла"),
				ui->edtLogFileName->text());
	ui->edtLogFileName->setText(logFName);
	ui->checkBoxLogFile->setChecked(!logFName.isEmpty());
	logFileOpen();
}

void Dialog::on_checkBoxLogFile_toggled(bool checked)
{
	Q_UNUSED(checked);
	logFileOpen();
}

void Dialog::slotTimerLoopTest()
{
	if(m_Modbus && ui->cbTimerLoopTest->isChecked())
	{
		LoopTest();
		QTimer::singleShot(ui->edtPause->value(), this, SLOT(slotTimerLoopTest()));
	}
}

void Dialog::slotTimerCustomRequest()
{
	if(m_Modbus && ui->cbTimerCustomRequest->isChecked())
	{
		customTest();
		QTimer::singleShot(ui->edtPause->value(), this, SLOT(slotTimerCustomRequest()));
	}
}

void Dialog::slotTimerWrite()
{
	if(m_Modbus && ui->cbTimerWrite->isChecked())
	{
		Write();
		QTimer::singleShot(ui->edtPause->value(), this, SLOT(slotTimerWrite()));
	}
}

void Dialog::slotTimerRead()
{
	if(ui->cbTimerRead->isChecked())
	{
		Read();
		QTimer::singleShot(ui->edtPause->value(), this, SLOT(slotTimerRead()));
	}
}

void Dialog::slotTimerListening()
{
	if(m_Modbus && ui->cbListeningOn->isChecked())
	{
		portListen();
		QTimer::singleShot(20, this, SLOT(slotTimerListening()));
	}
	else
		m_listeningBuf.clear();
}

void Dialog::on_comboPort_activated(int index)
{
	Q_UNUSED(index);
	if(m_Modbus)
		on_pushOpen_clicked();
}

void Dialog::on_cboxRouteType_activated(int index)
{
	ui->frameRoute->setVisible(index == 1);
	ui->frameRouteTCP->setVisible(index == 2);
	if(m_Modbus)
		on_pushOpen_clicked();
}

void Dialog::on_cbListeningOn_clicked()
{
	if(chkPort())
		slotTimerListening();
}

void Dialog::on_cbTimerCustomRequest_clicked()
{
	if(chkPort())
		slotTimerCustomRequest();
}

void Dialog::on_cbTimerWrite_clicked()
{
	if(chkPort())
		slotTimerWrite();
}

void Dialog::on_cbTimerRead_clicked()
{
	if(chkPort())
		slotTimerRead();
}

void Dialog::on_cbTimerLoopTest_clicked()
{
	if(chkPort())
		slotTimerLoopTest();
}



void Dialog::on_lineEditMaxLog_textChanged(const QString &arg1)
{
	m_logModel.setMaxSize(ui->lineEditMaxLog->text().toInt());
}
