#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include "qmyvalidator.h"
#include "PortSetting.h"
#include "livnyparser.h"
#include "loglistmodel.h"

#include <QSettings>
#include <QDialog>
#include <QTimer>
#include <QFile>
#include <QListWidget>
#include <QVector>
#include <QClipboard>
#include <QStringListModel>

#include "../modb_lib/include/syncmb_master.h"

namespace Ui
{
class Dialog;
}

int StrToArr(QString& str, QByteArr& arr);

enum packetType
{
	ptUnknown,
	ptEmpty,
	ptWaitMoreBytes,
	ptRequest,
	ptAnswer
};


class Dialog : public QDialog
{
	Q_OBJECT

public:
	Dialog(QWidget* parent = 0);
	~Dialog();

	void logFileOpen();
	void closePort();
private:
	Ui::Dialog* ui;
	QString m_sNumPort;

	QHexValidator* pHexValidate;
	QDecValidator* pDecValidate;

private:
	QPortSet m_portSet;
	QMBSyncPort* m_Modbus;
	Qt::CheckState m_Scrool;
	Qt::CheckState m_OneReg;

	QByteArray m_listeningBuf;
	QLogFile* m_logFile;
	packetType m_prevPacketType;
	int m_prevSize;

	QVector<QString> m_History;
	LivnyParser m_livParser;

	LogListModel m_logModel;

protected:
	bool eventFilter(QObject* obj, QEvent* event);

	bool chkPort();
private slots:
	void on_tabWidget_currentChanged(int index);
	void on_comboBox_8_currentIndexChanged(int index);
	void on_pushButton_4_pressed();
	void on_checkBox_Singl_stateChanged(int);
	void on_pushButton_3_clicked();
	void on_pushButton_Setup_clicked();
	void on_pushButton_2_clicked();
	void on_checkScrList_stateChanged(int);
	void on_pushButton_pressed();
	void on_pushClose_clicked();
	void on_pushOpen_clicked();
	void Logger(const QString& str, bool incTimestamp = true);
	void on_checkHex_clicked();
	void on_checkHex_2_clicked();
	void on_userFuncButton_clicked();
	void on_checkHex_3_clicked();
	void on_listHistory_itemDoubleClicked(QListWidgetItem* item);
	void on_listHistory_itemClicked(QListWidgetItem* item);
	void on_pushButton_Edit_clicked();
	void on_pushButton_5_clicked();
	void on_edtTimeout_valueChanged(int);
	void slotTimerListening();
	void on_pushButtonLogFileSelect_clicked();
	void on_checkBoxLogFile_toggled(bool checked);
	void slotTimerLoopTest();
	void slotTimerCustomRequest();
	void slotTimerWrite();
	void slotTimerRead();
	void on_comboPort_activated(int index);
	void on_cboxRouteType_activated(int index);

	void on_cbListeningOn_clicked();

	void on_cbTimerCustomRequest_clicked();

	void on_cbTimerWrite_clicked();

	void on_cbTimerRead_clicked();

	void on_cbTimerLoopTest_clicked();

	void on_lineEditMaxLog_textChanged(const QString &arg1);

private:
	//Функция чтения регистров
	uint Read(void);
	//Функция записи регистров
	uint Write(void);
	//Произвольная команда
	void customTest();
	//Диагностическая функция
	uint LoopTest(void);
	void RefreshLblPort(void);

	void addToHistory(const QString& str);
	void saveHistory();
	void saveLog();
	void loadHistory();
	void portListen();

public slots:
	void onRTURequest(const QString req);
	void onRTUResponse(const QString req);
	void onTCPRequest(quint32 peerIP, const QString& recv);
	void onTCPResponse(quint32 peerIP, const QString& resp);
	void onError(const QString req);
};

#endif // MAINDIALOG_H
