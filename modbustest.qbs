import qbs

Application {

    name: "modbustest"

    property bool isWindows: qbs.targetOS.contains("windows")
    property bool isLinux: qbs.targetOS.contains("linux")
    property bool isDebug: qbs.buildVariant == "debug"

    Depends { name: "cpp" }
    Depends { name: "Qt.core"}
    Depends { name: "Qt.network"}
    Depends { name: "Qt.widgets"}
    Depends { name: "Qt.gui"}

    cpp.includePaths: ["../modb_lib/include", "../modb_lib/src", "../modb_lib/src/func"]
	cpp.libraryPaths: ["../../qbuild/inst_" + profile + (isDebug ? "_d" : "_r")]
    cpp.dynamicLibraries: [isDebug ? "modb_lib_d" : "modb_lib"]

    consoleApplication: false

    cpp.defines: [
        (isWindows ? "WIN32" : ""),
        (isDebug ? "DEBUG" : "RELEASE")]

    files: [
        "PortSetting.cpp",
        "PortSetting.h",
        "PortSetting.ui",
        "livnyparser.cpp",
        "livnyparser.h",
        "loglistmodel.cpp",
        "loglistmodel.h",
        "main.cpp",
        "maindialog.cpp",
        "maindialog.h",
        "maindialog.ui",
        "qmyvalidator.cpp",
        "qmyvalidator.h",
    ]

    destinationDirectory: "../../inst_" + profile + (isDebug ? "_d" : "_r")

}
