<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="ru">
<context>
    <name>Dialog</name>
    <message>
        <location filename="maindialog.ui" line="25"/>
        <source>ModbusTest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="37"/>
        <source>COM-порт:</source>
        <translation>COM-port:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="60"/>
        <location filename="maindialog.ui" line="1186"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="86"/>
        <source>Открыть</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="102"/>
        <source>Закрыть</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="126"/>
        <source>Инкапсуляция:</source>
        <translation>Incapsulation:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="140"/>
        <source>нет</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="145"/>
        <source>инкапсуляция</source>
        <translation>incapsulation</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="150"/>
        <source>шлюз TCP - RTU</source>
        <translation>gate TCP-RTU</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="173"/>
        <location filename="maindialog.ui" line="243"/>
        <source>Адрес маршрутизатора:</source>
        <translation>Gate address:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="201"/>
        <source>192.168.0.247</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="271"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="298"/>
        <source>Номер порта:</source>
        <translation>Port number:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="395"/>
        <source>Адрес устройства:</source>
        <translation>Device address:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="424"/>
        <location filename="maindialog.ui" line="709"/>
        <location filename="maindialog.ui" line="842"/>
        <source>Hex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="448"/>
        <source>Таймаут:</source>
        <translation>Timeout:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="480"/>
        <source>Пауза:</source>
        <translation>Pause:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="577"/>
        <source>Диагностика(0x08)</source>
        <translation>Diagnosis(0x08)</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="583"/>
        <source>Подфункция:</source>
        <translation>Sub-function:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="609"/>
        <source>Значение счетчика:</source>
        <translation>Counter value:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="648"/>
        <location filename="maindialog.ui" line="791"/>
        <location filename="maindialog.ui" line="941"/>
        <location filename="maindialog.ui" line="1004"/>
        <source>Запрос</source>
        <translation>Query</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="655"/>
        <location filename="maindialog.ui" line="798"/>
        <location filename="maindialog.ui" line="948"/>
        <location filename="maindialog.ui" line="1011"/>
        <source>Постоянно</source>
        <translation>Repeat continously</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="663"/>
        <source>Чтение(0x03)</source>
        <translation>Read(0x03)</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="671"/>
        <location filename="maindialog.ui" line="816"/>
        <source>Нач. регистр:</source>
        <translation>Start register:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="729"/>
        <source>Кол-во регистров:</source>
        <translation>Register count:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="772"/>
        <location filename="maindialog.ui" line="909"/>
        <source>Значение:</source>
        <translation>Value:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="808"/>
        <source>Запись(0x10,0x06)</source>
        <translation>Write(0x10,0x06)</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="862"/>
        <source>К-во регистров:</source>
        <translation>Register count:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="885"/>
        <source>Один регистр (функция 6)</source>
        <translation>Single register (func 6)</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="958"/>
        <source>Произвольная команда</source>
        <translation>Custom command</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="964"/>
        <source>Функция:</source>
        <translation>Function:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="984"/>
        <source>Данные:</source>
        <translation>Data:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="1019"/>
        <source>Прослушивание</source>
        <translation>Listening</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="1025"/>
        <source>Включить режим прослушивания</source>
        <translation>Enable listening mode</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="1041"/>
        <source>Редактировать</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="1111"/>
        <source>Прокручивать список</source>
        <translation>Scroll list</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="1134"/>
        <source>Макс. кол-во строк на экране:</source>
        <translation>Max. log lines:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="1153"/>
        <source>1000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="1163"/>
        <source>Лог-файл:</source>
        <translation>Log-file:</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="1209"/>
        <source>Скопировать выделенное</source>
        <translation>Copy selected</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="1216"/>
        <source>Очистить</source>
        <translation>Clear</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="115"/>
        <source>Эхо</source>
        <translation>Echo</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="118"/>
        <source>Сброс счетчиков</source>
        <translation>Reset counters</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="121"/>
        <source>Количество пакетов</source>
        <translation>Packet count</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="124"/>
        <source>Ошибки CRC</source>
        <translation>CRC errors</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="127"/>
        <source>Ошибки шины</source>
        <translation>Bus errors</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="130"/>
        <source>Slave message count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="133"/>
        <source>Slave No response count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="136"/>
        <source>Slave NAK count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="359"/>
        <location filename="maindialog.cpp" line="1139"/>
        <source>Ошибка</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="359"/>
        <source>Ошибка записи лог-файла: %1</source>
        <translation>Error writing log file:</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="455"/>
        <source>Успешно открыт порт </source>
        <translation>Port open successfully </translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="460"/>
        <location filename="maindialog.cpp" line="906"/>
        <source>Запрос %1</source>
        <translation>Query %1</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="465"/>
        <location filename="maindialog.cpp" line="903"/>
        <source>Ответ %1</source>
        <translation>Answer %1</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="470"/>
        <source>Запрос %1 %2</source>
        <translation>Query %1 %2</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="476"/>
        <source>Ответ %1 %2</source>
        <translation>Answer %1 %2</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="481"/>
        <source>Ошибка: %1</source>
        <translation>Error: %1</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="566"/>
        <source>Ошибка чтения - </source>
        <translation>Read error - </translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="676"/>
        <source>Ошибка записи - </source>
        <translation>Write error - </translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="681"/>
        <location filename="maindialog.cpp" line="784"/>
        <source>Неверные данные</source>
        <translation>Wrong data</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="727"/>
        <source>Ошибка - %1</source>
        <translation>Error - %1</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="777"/>
        <source>Ошибка - </source>
        <translation>Error - </translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="804"/>
        <source>Закрыт порт </source>
        <translation>Port closed </translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="900"/>
        <source>Пакет %1</source>
        <translation>Packet %1</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="1139"/>
        <source>Ошибка открытия лог-файла: %1</source>
        <translation>Error opening log file: %1</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="1147"/>
        <source>Введите имя лог-файла</source>
        <translation>Enter log file name</translation>
    </message>
</context>
<context>
    <name>PortSetting</name>
    <message>
        <location filename="PortSetting.ui" line="31"/>
        <source>Параметры порта</source>
        <translation>Port settings</translation>
    </message>
    <message>
        <location filename="PortSetting.ui" line="76"/>
        <source>Скорость:</source>
        <translation>Baud rate:</translation>
    </message>
    <message>
        <location filename="PortSetting.ui" line="122"/>
        <source>Четность:</source>
        <translation>Parity:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="29"/>
        <source>сборка от %1 в %2</source>
        <translation>build %1 at %2</translation>
    </message>
</context>
</TS>
