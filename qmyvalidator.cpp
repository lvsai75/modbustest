#include "qmyvalidator.h"

QHexValidator::QHexValidator(QObject* parent)
    : QValidator(parent)
{
}

QValidator::State QHexValidator::validate(QString& str, int&) const
{
    QValidator::State rez = Invalid;
    QRegExp rxp = QRegExp("[^0-9A-Fa-f]");

    if(!str.contains(rxp))
    {
        rez = Acceptable;
    }

    return rez;
}

QDecValidator::QDecValidator(QObject* parent)
    : QValidator(parent)
{
}

QValidator::State QDecValidator::validate(QString& str, int&) const
{
    QValidator::State rez = Invalid;
    QRegExp rxp = QRegExp("[^0-9]");

    if(!str.contains(rxp))
    {
        rez = Acceptable;
    }

    return rez;
}
