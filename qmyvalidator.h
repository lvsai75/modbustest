#ifndef QMYVALIDATOR_H
#define QMYVALIDATOR_H

#include <QValidator>

class QHexValidator : public QValidator
{
public:
    QHexValidator(QObject* parent);
    virtual QValidator::State validate(QString& str, int& par) const;
};

class QDecValidator : public QValidator
{
public:
    QDecValidator(QObject* parent);
    virtual QValidator::State validate(QString& str, int& par) const;
};

#endif // QMYVALIDATOR_H
